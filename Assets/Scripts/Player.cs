﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	private Rigidbody2D myRigidbody;
	
	[SerializeField]
	private float movementSpeed;
	private bool facingRight;
	public bool grounded = true;
	public float JumpPower = 190f;
	private int secondsUntilNextLevel = 1;

	
	void Start () {
		facingRight = true;
		myRigidbody = GetComponent<Rigidbody2D>();
	}
	
	
	void Update () {
		float horizontal = Input.GetAxis("Horizontal");
		HandleMovement(horizontal);
		Flip(horizontal);
		
		if (Input.GetButtonDown("Jump") && grounded) {
			myRigidbody.AddForce(Vector2.up * JumpPower);
			grounded = true;
		}
	}
	
	private void HandleMovement(float horizontal) {
		myRigidbody.velocity = new Vector2(horizontal * movementSpeed, myRigidbody.velocity.y);
		
	}
	
	private void Flip(float horizontal) {
		if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight) {
			facingRight = !facingRight;
			
			Vector3 theScale = transform.localScale;
			
			theScale.x *= -1;
			transform.localScale = theScale;
		}

	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if(objectPlayerCollidedWith.tag == "Exit")
		{
			Invoke("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		}
}
	private void LoadNewLevel()
	{
		Application.LoadLevel("Sunset");

	}
}